/***Tolulope Adetula**/

//set up markers 
var myMarkers = {"markers": [
        {"latitude": "6.428056", "longitude":"3.421944", "icon": "img/map_marker.png"}
    ]
};

//set up map options
$(".map_contact").mapmarker({
    zoom	: 17,
    center	: 'Victoria Island, Lagos',
    markers	: myMarkers
});

